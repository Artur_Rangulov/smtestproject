import {
    CLICK_HANDLER,
    CHANGE_YEAR,
    CHANGE_MODE,
} from "./actionTypes";

export function clickHandler(){
    return {
        type: CLICK_HANDLER
    }
}

export function changeYear(event){
    return {
        type: CHANGE_YEAR,
        year: event.target.value
    }
}

export function changeMode(event){
    return {
        type: CHANGE_MODE,
        mode: event.target.value,
    }
}