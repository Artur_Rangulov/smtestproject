import { CHANGE_YEAR, CHANGE_MODE } from "../actions/actionTypes";
import products from '../../data/data'

//функция возвращающая массив 
//с уникальными годами из входящего массива
const uniqueYears = products => { 
	var obj = {};
	for (var i = 0; i < products.length; i++) {
		var str = products[i].year;
		obj[str] = true;
	}
	return Object.keys(obj);
}

const years = uniqueYears(products)

//присвоение максимального года из products
//после сортировки по убыванию
const maxYear = +years.sort((a,b) => {return b - a})[0]

//выборка продуктов для первой отрисовки
let filteredproducts = products.filter( product => {
	product.data = [[product.feature1, product.feature2]]
	return +product.year === +maxYear
})

const filterProduct = (products, year, mode) => {

	switch (mode) {
		case 'Goods':
			return products && products.filter( product => {
				product.data = [[product.feature1, product.feature2]]
				return +product.year === +year
			})
		case 'Category':
			let filterdproductsDataMore150 = [], filterdproductsDataLess150 = [], filterdproductsOther = []
			products && products.forEach(product => {
				if(+product.year === +year){
					switch (true) {
						case product.feature1 >=150:
							filterdproductsDataMore150.push([product.feature1, product.feature2])
							break
						case product.feature1 <=100:
							filterdproductsDataLess150.push([product.feature1, product.feature2])
							break
						default:
							filterdproductsOther.push([product.feature1, product.feature2])
							break;
					}
				}
			})
			return [{
				name: 'feature1 больше 150',
				data: filterdproductsDataMore150,
			},{
				name: 'feature1 меньше 100',
				data: filterdproductsDataLess150,
			},{
				name: 'Другие товары',
				data: filterdproductsOther,
			}]
		default:
			break;
	}
}

const initialState = {
	years: years,
	year: maxYear,
	mode: 'Goods',
	series: filteredproducts,
}

export default function reducerChart (state = initialState, action){
	switch (action.type) {
	case CHANGE_YEAR:
		let filteredproducts = filterProduct(products, action.year, state.mode)
		return {
			...state,
			year: action.year,
			series: filteredproducts,
		}
	
	case CHANGE_MODE:
		let filterdproducts = filterProduct(products, state.year, action.mode)
			return {
			...state,
			series: filterdproducts,
			mode: action.mode,
		}
		default:
	return state
	}
}