import React, { Component } from 'react'
import { connect } from 'react-redux'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Select from '../../components/Select/Select'
import { changeYear, changeMode} from '../../store/actions/action';

const optionsSelectMode = [{
    value: 'Goods'
},{
    value: 'Category'
}]

class Chart extends Component{

  optionsYears = () => {
	return this.props.years && this.props.years.map(year => {
	    return {value: +year}
	})
  }
  render(){
    return(
      <div>
        <HighchartsReact
			highcharts={Highcharts}
			options={{
				chart: {type: 'scatter',zoomType: 'xy'},
				title: {text: 'Goods'},
				xAxis: {title: {text: 'Feature 1'}},
				yAxis: {title: {text: 'Feature 2'}},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'middle'
				},
				plotOptions: {
				  scatter: {
				    marker: {radius: 5,},
				    states: {
				    	hover: {marker: {enabled: false}}
				  	},
				    tooltip: {
				    	headerFormat: '<b>{series.name}</b><br>',
				    	pointFormat: '{point.x}, {point.y}'
				    }
				  }
				},
				series: this.props.series,
			}}
        />
        <Select options={this.optionsYears()} onChange={event => this.props.changeYear(event)} />
        <Select options={optionsSelectMode} onChange={event => this.props.changeMode(event)}/>
      </div>
    )
  }
}

function mapStateToProps(state){
    return {
        years: state.chart.years,
        series: state.chart.series,
    }
}

function mapDispathToProps(dispath){
    return{
        changeYear: event => dispath(changeYear(event)),
        changeMode: event => dispath(changeMode(event)),
    }
}

export default connect(mapStateToProps, mapDispathToProps)(Chart)