import React, { Component } from 'react'
import styles from './HomeStyle.scss'

export default class Home extends Component {
    render() {
        return (
            <div className={styles.home}>
                Hello!
            </div>
        )
    }
}
