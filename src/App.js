import React from 'react'
import styles from './AppStyle.scss'
import {HomeGlyph, DefaultGlyph} from './components/Glyph/Glyph'
import { BrowserRouter as Router, Route, NavLink, Switch, Redirect } from "react-router-dom"
import Chart from './containers/Chart/Chart'
import Home from './containers/Home/Home'

const links = [
  {link: '/', title:'Home', icon: <HomeGlyph />},
  {link: '/chart/', title:'Chart', icon: <DefaultGlyph />},
] 

const App = () => {
  return (
    <Router>
      <div className={styles.container}>
        <div className={styles.navigation}>
          <ul className={styles.navigation__items}>
          {
            links.map((item, index) => (
              <li className={styles.navigation__item} key={index}>
                <NavLink 
                  exact to={item.link}
                  className={styles.navigation__link}
                  activeClassName={styles.navigation__link_active}
                >
                  {item.icon}<span>{item.title}</span>
                </NavLink>
              </li>))
          }
          </ul>
        </div>
        <div className={styles.content}>
          <Switch>
            <Route path="/chart/" component={Chart} />
            <Route path="/" exact component={Home} />
            <Redirect to="/" />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App
