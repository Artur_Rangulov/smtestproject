import React from 'react'
import PropTypes from 'prop-types';

const Select = ({options, onChange}) => {
    return (
        <select className="custom-select" onChange={onChange}>
            {options
                ? options.map((option, index) => {
                   return <option key={index} value={option.value}>{option.value}</option>
                    })
                : null
            }
        </select>
    )
}

Select.propTypes = {
    options: PropTypes.array,
    onChange: PropTypes.func,
}

export default Select
