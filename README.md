This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Quick start guide

1. Run `npm i` to install all dependencies after cloning the repository 
2. Run the application using `npm start`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.<br>

Authors
===

#### Rangulov Artur
- ![gitlab](icons/gitlab.png) [Rangulov](https://gitlab.com/Artur_Rangulov)
- ![email](icons/email.png) [rangulov.artur@mail.ru](mailto:rangulov.artur@mail.ru)
- ![telegram](icons/telegram.png) [@RangulovA](https://t.me/RangulovA)

